var clicado = false;
$('.btn-menu').click(function(){
    clicado = !clicado;
    if(clicado){
        $('.menu').slideDown(500);
        $("#btn-menu").css("color", "rgb(253, 119, 29)");
        $('#logo').css("color", "rgb(253, 119, 29)");
    }else{
        $('.menu').slideUp(500);
        $("#btn-menu").css("color", "#000");
        $('#logo').css("color", "#000");
    }
});

/* EFEITO SCROLL */
var $doc = $('html, body');
$('.scrollSuave').click(function() {
    $doc.animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top-100
    }, 500);
    return false;
});

var options = [
    {selector: '.logo', offset: 200, callback: function(el) { 
        Materialize.showStaggeredList($(el));
    } },
  ];
  Materialize.scrollFire(options);