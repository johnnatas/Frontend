/* CARROSSEL INFINITO */
$('.desc').owlCarousel({
    stagePadding: 3,
    loop:true,
    margin:0,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

$('#galeria').owlCarousel({
    stagePadding: Number,
    loop:true,
    margin:0,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});

var desc = $('.desc');
desc.owlCarousel();
var galeria = $('#galeria');
galeria.owlCarousel();

// Item posterior
$('.customNextBtn').click(function() {
    galeria.trigger('next.owl.carousel');
});
$('.customNextBtn').click(function() {
    desc.trigger('next.owl.carousel');
});
// Item anterior
$('.customPrevBtn').click(function() {
    galeria.trigger('prev.owl.carousel', [300]);
});
$('.customPrevBtn').click(function() {
    desc.trigger('prev.owl.carousel', [300]);
});

/* CORPO-CLINICO CARROSSEL */
$(document).ready(function(){
    flag=1;
     $("#nex").click(function(){
        if(flag==0)
         {
             $("#side1").css("z-index","999");
             $("#side2").css("z-index","9");
             $("#side3").css("z-index","9");
             $("#side1").css("transform","translateX(0px) scale(1.5)");
             $("#side2").css("transform","translateX(-100px) scale(1)");
             $("#side3").css("transform","translateX(100px)");
             $("#side2").css("opacity", "0.5");
             $("#side3").css("opacity", "0.5");
             $("#side1").css("opacity", "1");
             flag=1;
         }
         else if(flag==1)
         {
             $("#side3").css("z-index","999");
             $("#side2").css("z-index","9");
             $("#side1").css("z-index","9");
             $("#side3").css("transform","translateX(0px) scale(1.5)");
             $("#side1").css("transform","translateX(-100px) scale(1)");
             $("#side2").css("transform","translateX(100px)");
             $("#side1").css("opacity", "0.5");
             $("#side2").css("opacity", "0.5");
             $("#side3").css("opacity", "1");
             flag=2;
         }
         else if(flag==2)
         {
             $("#side2").css("z-index","999");
             $("#side3").css("z-index","9");
             $("#side1").css("z-index","9");
             $("#side2").css("transform","translateX(0px) scale(1.5)");
             $("#side3").css("transform","translateX(-100px) scale(1)");
             $("#side1").css("transform","translateX(100px)");
             $("#side3").css("opacity", "0.5");
             $("#side1").css("opacity", "0.5");
             $("#side2").css("opacity", "1");
             flag=0;
         }
     });
     $("#pre").click(function(){
        if(flag==0)
         {
             $("#side3").css("z-index","999");
             $("#side2").css("z-index","9");
             $("#side1").css("z-index","9");
             $("#side3").css("transform","translateX(0px) scale(1.5)");
             $("#side1").css("transform","translateX(-100px) scale(1)");
             $("#side2").css("transform","translateX(100px)");
             $("#side1").css("opacity", "0.5");
             $("#side2").css("opacity", "0.5");
             $("#side3").css("opacity", "1");
             flag=2;
         }
         else if(flag==1)
         {
             $("#side2").css("z-index","999");
             $("#side3").css("z-index","9");
             $("#side1").css("z-index","9");
             $("#side2").css("transform","translateX(0px) scale(1.5)");
             $("#side3").css("transform","translateX(-100px) scale(1)");
             $("#side1").css("transform","translateX(100px)");
             $("#side3").css("opacity", "0.5");
             $("#side1").css("opacity", "0.5");
             $("#side2").css("opacity", "1");
             flag=0;
         }
         else if(flag==2)
         {
             $("#side1").css("z-index","999");
             $("#side2").css("z-index","9");
             $("#side3").css("z-index","9");
             $("#side1").css("transform","translateX(0px) scale(1.5)");
             $("#side2").css("transform","translateX(-100px) scale(1)");
             $("#side3").css("transform","translateX(100px)");
             $("#side2").css("opacity", "0.5");
             $("#side3").css("opacity", "0.5");
             $("#side1").css("opacity", "1");
             flag=1;
         }
     }); 

 });

/* MENU */
$('.btn-open-menu').click(function(){
    $('.menu-hidden').toggle("fade");
});

$('.btn-agenda').click(function(){
    $('.agendar').toggle("slow");
});

/* EFEITO SCROLL */
var $doc = $('html, body');
$('.scrollSuave').click(function() {
    $doc.animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top-100
    }, 500);
    return false;
});